# What This Does / Modules

## Configuration APIs
	1.users
	2.feature switches
	3.authorizations
	4.schedule tasks

## Listen from Telegram (w/ Auth)

## Listen from other Webhook (w/ Auth)

## Send to Telegram

## Send to other Webhook

## Schedule Task

## Logging

## Generate Report (w/ Auth)

# Design

## Exception Handling
    1. BizException
    2. NotifyException
    3. UnknownException

## Logging