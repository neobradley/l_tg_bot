package me.neobradley.lab.domain.demo.service;

import lombok.extern.slf4j.Slf4j;
import me.neobradley.lab.domain.demo.model.HelloWorldMessage;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class HelloWorldServiceTest {

    @Resource
    private HelloWorldService helloWorldService;

    @Test
    public void sayHelloWorld() {
        HelloWorldMessage helloWorldMessage = helloWorldService.sayHelloWorld();
        Assert.assertEquals(mockData().getMessage(), helloWorldMessage.getMessage());
    }

    private static HelloWorldMessage mockData() {
        HelloWorldMessage message = new HelloWorldMessage();
        message.setMessage("Hello World");
        return message;
    }
}