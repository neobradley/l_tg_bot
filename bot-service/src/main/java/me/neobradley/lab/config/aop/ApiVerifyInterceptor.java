package me.neobradley.lab.config.aop;

import lombok.extern.slf4j.Slf4j;
import me.neobradley.lab.exception.BizException;
import me.neobradley.lab.exception.ExceptionType;
import me.neobradley.lab.exception.NotifyException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class ApiVerifyInterceptor implements HandlerInterceptor {

    @Value("${swagger.switch:true}")
    boolean hasSwagger;

    @Value("${restful.signature.required:false}")
    boolean isSignatureRequired;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.debug("ApiVerifyInterceptor begin");
        boolean result = false;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            if (!hasSwagger && isAccessSwagger(request)) throw new BizException(ExceptionType.SWAGGER_OFF);
            if (hasSwagger && isAccessSwagger(request)) throw new NotifyException();
            if (isAccessSysPage(request)) throw new NotifyException();
        } catch (BizException be) {
            log.warn(be.getMessage());
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
        } catch (NotifyException ne) {
            result = true;
        }
        stopWatch.stop();
        log.debug("ApiVerifyInterceptor end -> spend {} ms", stopWatch.getTotalTimeMillis());
        return result;
    }

    private Boolean isAccessSwagger(HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        log.debug("isAccessSwagger hasSwagger -> {}; requestURI -> {}", hasSwagger, requestURI);
        return requestURI.contains("/swagger-resources") || "/swagger-ui.html".equals(requestURI);
    }

    private Boolean isAccessSysPage(HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        return requestURI.contains("/error");
    }
}
