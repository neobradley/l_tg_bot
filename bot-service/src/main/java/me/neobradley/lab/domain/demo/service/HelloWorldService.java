package me.neobradley.lab.domain.demo.service;

import lombok.extern.slf4j.Slf4j;
import me.neobradley.lab.domain.demo.model.HelloWorldMessage;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class HelloWorldService {

    public HelloWorldMessage sayHelloWorld() {
        HelloWorldMessage message = new HelloWorldMessage();
        message.setMessage("Hello World");
        return message;
    }
}
