package me.neobradley.lab.domain.demo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.neobradley.lab.domain.demo.model.HelloWorldMessage;
import me.neobradley.lab.domain.demo.service.HelloWorldService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Api(value = "For demo purpose", tags = "Demo API")
@RequestMapping("/hello-world")
public class HelloWorldController {

    @Resource
    HelloWorldService helloWorldService;

    @ApiOperation("Demo hello world message")
    @GetMapping("/")
    public HelloWorldMessage sayHelloWorld() {
        return helloWorldService.sayHelloWorld();
    }
}

