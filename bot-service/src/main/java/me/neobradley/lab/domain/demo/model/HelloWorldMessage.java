package me.neobradley.lab.domain.demo.model;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
@ApiModel("Say something about hello world")
public class HelloWorldMessage implements Serializable {

    private static final long serialVersionUID = 7668339248056648966L;

    String message;
}
