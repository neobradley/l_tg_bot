package me.neobradley.lab.exception;

import java.util.Arrays;
import java.util.List;

public enum ExceptionType {
    UNKNOWN_EXCEPTION("This is unknown exception"),
    NOT_SUPPORTED_EXCEPTION("This is not supported in new framework"),
    INVALID_INPUT("Invalid incoming parameter"),
    SWAGGER_OFF("Access switched off swagger"),
    SIGNATURE_MISSSING("Request object without signature"),
    INVALID_SIGNATURE("Request object without valid signature");

    private String message;

    private List values;

    ExceptionType(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public List getValues() {
        return values;
    }

    public void setValues(Object... values) {
        Arrays.asList(values).addAll(this.values);
    }


    @Override
    public String toString() {
        return message + (getValuesToString() != null ? getValuesToString() : "");
    }

    private String getValuesToString() {
        if (values == null) return null;
        StringBuilder builder = new StringBuilder();
        values.forEach(item -> builder.append("value : ").append(item).append("; "));
        return builder.toString();
    }
}
