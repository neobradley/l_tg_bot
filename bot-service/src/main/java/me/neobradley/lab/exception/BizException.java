package me.neobradley.lab.exception;

public class BizException extends RuntimeException {

    private final ExceptionType exceptionType;

    public BizException(ExceptionType exceptionType) {
        super(exceptionType.getMessage());
        this.exceptionType = exceptionType;
    }

    public BizException(ExceptionType exceptionType, Throwable cause) {
        this(exceptionType);
        this.initCause(cause);
    }

    public BizException(String message, Throwable cause) {
        super(message, cause);
        throw new BizException(ExceptionType.NOT_SUPPORTED_EXCEPTION);
    }

    public BizException(Throwable cause) {
        super(cause);
        throw new BizException(ExceptionType.NOT_SUPPORTED_EXCEPTION);
    }

    public BizException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        throw new BizException(ExceptionType.NOT_SUPPORTED_EXCEPTION);
    }

    @Override
    public String getMessage() {
        return exceptionType.toString();
    }

    public ExceptionType getExceptionType() {
        return exceptionType;
    }
}
