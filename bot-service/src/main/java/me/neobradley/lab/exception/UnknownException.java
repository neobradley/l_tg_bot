package me.neobradley.lab.exception;

public class UnknownException extends BizException {

    public UnknownException() {
        super(ExceptionType.UNKNOWN_EXCEPTION);
    }

    public UnknownException(Throwable cause) {
        super(ExceptionType.UNKNOWN_EXCEPTION, cause);
    }
}
