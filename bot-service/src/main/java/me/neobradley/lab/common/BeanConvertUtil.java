package me.neobradley.lab.common;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class BeanConvertUtil {

    private static ObjectMapper mapper = new ObjectMapper();

    /**
     * 复制对像信息到指定类型对像中
     */
    public static <T> T copyBean(Object target, Class<T> cls) {
        log.debug("copyBean {}===>{}", target, cls);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        if (target == null) return null;
        try {
            return mapper.readValue(mapper.writeValueAsString(target), cls);
        } catch (IOException e) {
            return null;
        }
    }
}
