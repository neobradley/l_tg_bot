package me.neobradley.lab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LTBotApplication {

    public static void main(String[] args) {
        SpringApplication.run(LTBotApplication.class, args);
    }

}
