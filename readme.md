# L Telegram Bot

## Objective

* To enhance understanding of Spring Boot framework and practice of micro-service
* To experiment the idea of ChatOps

## Environment and Setup

* JDK 8 (I am working on upgrading 11, please have patience) [Download](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* Jetbrain IntelliJ [Download](https://www.jetbrains.com/idea/download/)
* Telegram [Download](https://telegram.org/)

## IntelliJ Setting

* Install IntelliJ Plugin
    * Lombok
    * Save Action
    * GenerateSerialVersionUID (optional)

* Other Setting
    * Build, Execution, Deployment -> Compiler -> Annotation Processor
        * Enable annotation processing (Check)
    * Editor -> Code Style -> Java
        * Imports
            * Class count to use import with * (999)
            * Name count to use static import with * (999)

# Project Config

/bot-service/conf/local/resources/application.yml
```yaml
logging:
  config: classpath:logback-boot.xml
spring:
  profiles:
    active: main
---
spring:
  profiles: main
server:
  port: 8080 # Change this if you want to use different port
swagger:
  switch: true # Change this to false if you do not want to use Swagger
restful:
  signature:
    required: false
logging:
  path: /var/log/app/ # Change this to dir you want to save log
  level:
    root: info # Change this to level of log for runtime
```

# Start Service
* Init Gradle
    * Gradle panel -> refresh icon (Double click)
* Build
    * Gradle panel -> :bot-service -> Tasks -> build -> build (Double click)
* Run
    * Gradle panel -> :bot-service -> Tasks -> application -> run (Double Click)

# Default Service

    Demo
        ${domain}/hello-world

    Actuator (Can do simple health check)
        ${domain}/actuator

    Swagger (API blueprint and document)
        ${domain}/swagger-ui.html
        ${domain}/v2/api-docs

# Reference
* [Gradle](https://gradle.org/)  - Automation tool for building project
* [Spring Boot](https://spring.io/projects/spring-boot) - Core framework
* [Undertow](http://undertow.io/) - High performance web server
* [Actuator](https://www.baeldung.com/spring-boot-actuators) - To expose operational information about the running application
* [Swagger](https://swagger.io/) - Tools to help developers design, build, document, and consume RESTful Web services
